package pl.goxy.minecraft.compass;

import pl.goxy.minecraft.compass.configuration.MenuConfiguration;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MenuService
{
    private final Map<String, MenuConfiguration> menuMap = new HashMap<>();

    public void add(MenuConfiguration configuration)
    {
        this.menuMap.put(configuration.getName().toLowerCase(Locale.ROOT), configuration);
    }

    public MenuConfiguration get(String name)
    {
        return this.menuMap.get(name.toLowerCase(Locale.ROOT));
    }
}
