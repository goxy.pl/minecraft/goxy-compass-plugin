package pl.goxy.minecraft.compass.configuration.position;

public interface ItemPosition
{
    int getSlot(int length, int height);

    int getX(int length, int height);

    int getY(int length, int height);
}
