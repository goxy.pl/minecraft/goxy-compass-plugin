package pl.goxy.minecraft.compass.configuration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class MenuConfiguration
{
    private final String name;
    private String title;
    private int height;
    private ItemConfiguration background;
    private final List<ItemConfiguration> items = new ArrayList<>();
    private String type;

    public MenuConfiguration(String name)
    {
        this.name = name;
    }

    @NotNull
    public String getName()
    {
        return name;
    }

    @NotNull
    public String getTitle()
    {
        return title;
    }

    public void setTitle(@NotNull String title)
    {
        this.title = title;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    @Nullable
    public ItemConfiguration getBackground()
    {
        return background;
    }

    public void setBackground(@Nullable ItemConfiguration background)
    {
        this.background = background;
    }

    @NotNull
    public List<ItemConfiguration> getItems()
    {
        return items;
    }

    @NotNull
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
