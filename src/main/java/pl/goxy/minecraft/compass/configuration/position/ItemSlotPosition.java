package pl.goxy.minecraft.compass.configuration.position;

public class ItemSlotPosition
        implements ItemPosition
{
    private final int slot;

    public ItemSlotPosition(int slot)
    {
        this.slot = slot;
    }

    @Override
    public int getSlot(int length, int height)
    {
        return slot;
    }

    @Override
    public int getX(int length, int height)
    {
        return this.slot % length;
    }

    @Override
    public int getY(int length, int height)
    {
        return this.slot / length;
    }
}
