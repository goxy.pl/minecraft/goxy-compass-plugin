package pl.goxy.minecraft.compass.configuration.position;

public class ItemCoordinatePosition
        implements ItemPosition
{
    private final int x;
    private final int y;

    public ItemCoordinatePosition(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    @Override
    public int getSlot(int length, int height)
    {
        return this.y * length + this.x;
    }

    @Override
    public int getX(int length, int height)
    {
        if (this.x < 0)
        {
            return this.x + length;
        }
        return this.x;
    }

    @Override
    public int getY(int length, int height)
    {
        if (this.y < 0)
        {
            return this.y + height;
        }
        return this.y;
    }
}
