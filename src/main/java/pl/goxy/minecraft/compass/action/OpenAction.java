package pl.goxy.minecraft.compass.action;

import com.github.stefvanschie.inventoryframework.gui.type.util.Gui;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import pl.goxy.minecraft.compass.GuiService;
import pl.goxy.minecraft.compass.MenuService;
import pl.goxy.minecraft.compass.configuration.MenuConfiguration;

import java.util.function.Function;
import java.util.logging.Logger;

public class OpenAction
        implements ItemAction
{
    private final Logger logger;
    private final MenuService menuService;
    private final GuiService guiService;
    private final String menuName;

    public OpenAction(Logger logger, MenuService menuService, GuiService guiService, String menuName)
    {
        this.logger = logger;
        this.menuService = menuService;
        this.guiService = guiService;
        this.menuName = menuName;
    }

    @Override
    public void accept(Player player)
    {
        MenuConfiguration configuration = this.menuService.get(this.menuName);
        if (configuration == null)
        {
            this.logger.warning("Configuration name '" + this.menuName + "' not found");
            return;
        }
        player.closeInventory();

        Gui menu = this.guiService.getMenu(configuration, player);
        menu.show(player);
    }

    @Override
    public Function<String, String> getReplacer()
    {
        return Function.identity();
    }
}
