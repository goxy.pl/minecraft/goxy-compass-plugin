package pl.goxy.minecraft.compass.action.connect.goxy;

import pl.goxy.minecraft.api.network.GoxyContainer;
import pl.goxy.minecraft.api.network.NetworkManager;
import pl.goxy.minecraft.api.player.PlayerStorage;

import java.util.function.Function;

public class ContainerConnectAction
        extends ConnectorConnectAction
{
    private final NetworkManager networkManager;

    public ContainerConnectAction(NetworkManager networkManager, PlayerStorage playerStorage, String name)
    {
        super(playerStorage, networkManager::getContainer, name);
        this.networkManager = networkManager;
    }

    @Override
    public Function<String, String> getReplacer()
    {
        return text ->
        {
            GoxyContainer container = this.networkManager.getContainer(this.getConnectorName());

            int onlinePlayers = 0;
            int maxPlayers = 0;
            if (container != null)
            {
                onlinePlayers = container.getOnlinePlayers();
                maxPlayers = container.getMaxPlayers();
            }

            return text.replace("{online_players}", String.valueOf(onlinePlayers))
                    .replace("{max_players}", String.valueOf(maxPlayers));
        };
    }
}
