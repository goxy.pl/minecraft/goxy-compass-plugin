package pl.goxy.minecraft.compass.action.connect.goxy;

import pl.goxy.minecraft.api.network.GoxyServer;
import pl.goxy.minecraft.api.network.NetworkManager;
import pl.goxy.minecraft.api.player.PlayerStorage;

import java.util.function.Function;

public class ServerConnectAction
        extends ConnectorConnectAction
{
    private final NetworkManager networkManager;

    public ServerConnectAction(NetworkManager networkManager, PlayerStorage playerStorage, String serverName)
    {
        super(playerStorage, networkManager::getServer, serverName);
        this.networkManager = networkManager;
    }

    @Override
    public Function<String, String> getReplacer()
    {
        return text ->
        {
            GoxyServer server = this.networkManager.getServer(this.getConnectorName());

            int onlinePlayers = 0;
            int maxPlayers = 0;
            if (server != null)
            {
                onlinePlayers = server.getOnlinePlayers();
                maxPlayers = server.getMaxPlayers();
            }

            return text.replace("{online_players}", String.valueOf(onlinePlayers))
                    .replace("{max_players}", String.valueOf(maxPlayers));
        };
    }
}
