package pl.goxy.minecraft.compass.action.connect.goxy;

import org.bukkit.entity.Player;
import pl.goxy.minecraft.api.network.GoxyConnector;
import pl.goxy.minecraft.api.player.GoxyPlayer;
import pl.goxy.minecraft.api.player.PlayerStorage;
import pl.goxy.minecraft.compass.action.ConnectAction;

import java.util.function.Function;

public abstract class ConnectorConnectAction
        implements ConnectAction
{
    private final PlayerStorage playerStorage;
    private final Function<String, GoxyConnector> connectorFunction;
    private final String connectorName;

    public ConnectorConnectAction(PlayerStorage playerStorage, Function<String, GoxyConnector> connectorFunction,
            String connectorName)
    {
        this.playerStorage = playerStorage;
        this.connectorFunction = connectorFunction;
        this.connectorName = connectorName;
    }

    @Override
    public void accept(Player player)
    {
        GoxyPlayer goxyPlayer = this.playerStorage.getPlayer(player.getUniqueId());
        if (goxyPlayer == null)
        {
            //should never happens
            return;
        }

        GoxyConnector connector = this.connectorFunction.apply(this.connectorName);
        if (connector != null)
        {
            connector.connect(goxyPlayer);
        }
        else
        {
            player.sendMessage("Nie znaleziono takiego serwera");
        }
    }

    public String getConnectorName()
    {
        return connectorName;
    }
}
