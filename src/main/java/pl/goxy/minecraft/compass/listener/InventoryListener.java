package pl.goxy.minecraft.compass.listener;

import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import pl.goxy.minecraft.compass.GuiService;
import pl.goxy.minecraft.compass.configuration.InventoryConfiguration;
import pl.goxy.minecraft.compass.configuration.ItemConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;

public class InventoryListener
        implements Listener
{
    private final Map<ItemConfiguration, UUID> itemIdMap = new HashMap<>();
    private final Map<UUID, ItemConfiguration> idItemMap = new HashMap<>();

    private final GuiService guiService;
    private final InventoryConfiguration inventoryConfiguration;

    private final NamespacedKey itemKey;

    public InventoryListener(GuiService guiService, InventoryConfiguration inventoryConfiguration,
            NamespacedKey itemKey)
    {
        this.guiService = guiService;
        this.inventoryConfiguration = inventoryConfiguration;
        this.itemKey = itemKey;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
    public void onJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        PlayerInventory inventory = player.getInventory();

        if (this.inventoryConfiguration.isClear())
        {
            inventory.clear();
        }
        this.inventoryConfiguration.getItems().forEach(item ->
        {
            if (item.getShowPermission() != null && !player.hasPermission(item.getShowPermission()))
            {
                return;
            }
            UUID itemId = this.itemIdMap.computeIfAbsent(item, i -> UUID.randomUUID());
            this.idItemMap.put(itemId, item);

            ItemStack itemStack;
            if (item.getAction() != null)
            {
                itemStack = this.guiService.getItemStack(item, item.getAction().getReplacer());
            }
            else
            {
                itemStack = this.guiService.getItemStack(item, Function.identity());
            }
            ItemMeta meta = itemStack.getItemMeta();
            if (meta != null)
            {
                PersistentDataContainer container = meta.getPersistentDataContainer();
                container.set(this.itemKey, PersistentDataType.STRING, itemId.toString());

                itemStack.setItemMeta(meta);
            }
            if (item.getPosition() != null)
            {
                int length = 9;
                int height = inventory.getSize();
                inventory.setItem(item.getPosition().getSlot(length, height), itemStack);
            }
            else
            {
                inventory.addItem(itemStack);
            }
        });
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onRightClick(PlayerInteractEvent event)
    {
        if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK)
        {
            return;
        }
        Player player = event.getPlayer();
        ItemStack item = event.getItem();
        if (item == null)
        {
            return;
        }
        ItemMeta meta = item.getItemMeta();
        if (meta == null)
        {
            return;
        }
        String itemIdString = meta.getPersistentDataContainer().get(this.itemKey, PersistentDataType.STRING);
        if (itemIdString == null)
        {
            return;
        }
        UUID actionId = UUID.fromString(itemIdString);
        ItemConfiguration configuration = this.idItemMap.get(actionId);
        if (configuration == null || configuration.getAction() == null)
        {
            return;
        }
        event.setUseItemInHand(Event.Result.DENY);
        if (configuration.getClickPermission() != null && !player.hasPermission(configuration.getClickPermission()))
        {
            return;
        }
        configuration.getAction().accept(player);
    }
}
