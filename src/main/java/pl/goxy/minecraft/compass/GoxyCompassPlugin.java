package pl.goxy.minecraft.compass;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import pl.goxy.minecraft.api.GoxyApi;
import pl.goxy.minecraft.api.network.NetworkManager;
import pl.goxy.minecraft.api.player.PlayerStorage;
import pl.goxy.minecraft.compass.action.OpenAction;
import pl.goxy.minecraft.compass.action.connect.goxy.ContainerConnectAction;
import pl.goxy.minecraft.compass.action.connect.goxy.ServerConnectAction;
import pl.goxy.minecraft.compass.command.MenuCommand;
import pl.goxy.minecraft.compass.configuration.InventoryConfiguration;
import pl.goxy.minecraft.compass.configuration.ItemConfiguration;
import pl.goxy.minecraft.compass.configuration.MenuConfiguration;
import pl.goxy.minecraft.compass.configuration.position.ItemCoordinatePosition;
import pl.goxy.minecraft.compass.configuration.position.ItemSlotPosition;
import pl.goxy.minecraft.compass.listener.InventoryListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class GoxyCompassPlugin
        extends JavaPlugin
{
    private static final String DEFAULT_GUI_TYPE = "chest";

    private MenuService menuService;
    private GuiService guiService;
    private NetworkManager networkManager;
    private PlayerStorage playerStorage;

    @Override
    public void onEnable()
    {
        this.saveDefaultConfig();

        this.menuService = new MenuService();
        this.guiService = new GuiService();
        this.networkManager = GoxyApi.getNetworkManager();
        this.playerStorage = GoxyApi.getPlayerStorage();

        FileConfiguration config = this.getConfig();
        ConfigurationSection menuSection = config.getConfigurationSection("menu");
        if (menuSection != null)
        {
            menuSection.getKeys(false).forEach(menuId ->
            {
                ConfigurationSection currentMenuSection = menuSection.getConfigurationSection(menuId);
                if (currentMenuSection == null)
                {
                    return;
                }
                MenuConfiguration menuConfiguration = new MenuConfiguration(menuId);
                String title = currentMenuSection.getString("title");
                menuConfiguration.setTitle(title != null ? title : menuId);

                int height = currentMenuSection.getInt("chest.height", currentMenuSection.getInt("height", 0));
                menuConfiguration.setHeight(height);

                String type = currentMenuSection.getString("type", DEFAULT_GUI_TYPE);
                menuConfiguration.setType(type);

                ConfigurationSection backgroundSection = currentMenuSection.getConfigurationSection("background");
                if (backgroundSection != null)
                {
                    Map<String, Object> background = new HashMap<>();
                    backgroundSection.getKeys(false).forEach(k -> background.put(k, backgroundSection.get(k)));
                    menuConfiguration.setBackground(this.getItemConfiguration(background));
                }
                menuConfiguration.getItems().addAll(this.getItems(currentMenuSection));
                menuService.add(menuConfiguration);
            });
        }

        ConfigurationSection inventorySection = config.getConfigurationSection("inventory");
        InventoryConfiguration inventoryConfiguration;
        if (inventorySection != null)
        {
            inventoryConfiguration = new InventoryConfiguration();
            inventoryConfiguration.setClear(inventorySection.getBoolean("clear"));
            inventoryConfiguration.setItems(this.getItems(inventorySection));

            NamespacedKey key = new NamespacedKey(this, "item-id");
            InventoryListener listener = new InventoryListener(this.guiService, inventoryConfiguration, key);
            this.getServer().getPluginManager().registerEvents(listener, this);
        }

        Objects.requireNonNull(this.getCommand("menu")).setExecutor(new MenuCommand(this.menuService, this.guiService));
    }

    private List<ItemConfiguration> getItems(ConfigurationSection section)
    {
        List<ItemConfiguration> itemList = new ArrayList<>();
        if (section == null)
        {
            return itemList;
        }
        List<Map<?, ?>> items = section.getMapList("items");
        items.forEach(item ->
        {
            ItemConfiguration itemConfiguration = this.getItemConfiguration(item);
            if (itemConfiguration == null)
            {
                this.getLogger().warning("Cannot load item configuration");
                return;
            }
            itemList.add(itemConfiguration);
        });
        return itemList;
    }

    private ItemConfiguration getItemConfiguration(Map<?, ?> item)
    {
        Material type = Material.getMaterial((String) item.get("type"));
        if (type == null)
        {
            return null;
        }
        ItemConfiguration itemConfiguration = new ItemConfiguration(type);
        if (item.containsKey("slot"))
        {
            int slot = ((Number) item.get("slot")).intValue();
            itemConfiguration.setPosition(new ItemSlotPosition(slot));
        }
        else if (item.containsKey("x") && item.containsKey("y"))
        {
            int x = ((Number) item.get("x")).intValue();
            int y = ((Number) item.get("y")).intValue();
            itemConfiguration.setPosition(new ItemCoordinatePosition(x, y));
        }
        if (item.containsKey("name"))
        {
            itemConfiguration.setName((String) item.get("name"));
        }
        if (item.containsKey("lore"))
        {
            itemConfiguration.setLore((List<String>) item.get("lore"));
        }
        if (item.containsKey("open"))
        {
            String name = (String) item.get("open");
            itemConfiguration.setAction(new OpenAction(this.getLogger(), this.menuService, this.guiService, name));
        }
        else if (item.containsKey("connect"))
        {
            String name = (String) item.get("connect");
            String[] strings = name.split("=", 2);

            String connectorType = strings[0];
            String connectorName = strings.length > 1 ? strings[1] : null;
            if (connectorType.equalsIgnoreCase("server"))
            {
                itemConfiguration.setAction(
                        new ServerConnectAction(this.networkManager, this.playerStorage, connectorName));
            }
            else if (connectorType.equalsIgnoreCase("container"))
            {
                itemConfiguration.setAction(
                        new ContainerConnectAction(this.networkManager, this.playerStorage, connectorName));
            }
        }
        if (item.containsKey("permission"))
        {
            Map<?, ?> permission = (Map<?, ?>) item.get("permission");
            if (permission.containsKey("show"))
            {
                itemConfiguration.setShowPermission((String) permission.get("show"));
            }
            if (permission.containsKey("click"))
            {
                itemConfiguration.setClickPermission((String) permission.get("click"));
            }
        }
        return itemConfiguration;
    }
}
